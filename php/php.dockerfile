FROM php:8.3-fpm-alpine

ENV GROUP=laravel
ENV USER=laravel

RUN adduser -g ${GROUP} -s /bin/sh -D ${USER}

RUN sed -i "s/user = www-data/user = ${USER}/g" /usr/local/etc/php-fpm.d/www.conf
RUN sed -i "s/group = www-data/group = ${GROUP}/g" /usr/local/etc/php-fpm.d/www.conf

RUN mkdir -p /app

RUN apk add --no-cache --update \
    icu-dev \
    zlib-dev \
    libpng-dev \
    ${PHPIZE_DEPS} \
    linux-headers

RUN docker-php-ext-install pdo pdo_mysql opcache pcntl intl gd \
    && pecl install -o -f redis-6.0.2 && pecl install -o -f xdebug-3.3.2 \
    && docker-php-ext-enable redis xdebug

COPY php/xdebug.ini "${PHP_INI_DIR}/conf.d"

COPY php/docker-entrypoint.sh /docker-entrypoint.sh

RUN chmod +x /docker-entrypoint.sh

USER ${USER}

ENTRYPOINT [ "/docker-entrypoint.sh" ]