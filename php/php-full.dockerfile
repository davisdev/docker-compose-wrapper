FROM composer:2.7.2 as composer-build

WORKDIR /app

COPY src/composer.json src/composer.lock /app/

RUN composer install \
    --no-autoloader \
    --ignore-platform-reqs \
    --no-progress \
    --no-scripts

# Node dependencies
FROM node:21.7.3-alpine as node-build

WORKDIR /app

# For InertiaJS related projects, this need to be copied in.
#COPY --from=composer-build /app/vendor/tightenco/ziggy /app/vendor/tightenco/ziggy/

# NB: Make sure to not use symlinks (not supported and won't be)
COPY src/package.json src/package-lock.json src/vite.config.js /app/
COPY src/resources /app/resources/
COPY src/public /app/public/

RUN npm ci
RUN npm run build

# Main
FROM php:8.3-fpm-alpine

ENV GROUP=laravel
ENV USER=laravel

RUN adduser -g ${GROUP} -s /bin/sh -D ${USER}

RUN sed -i "s/user = www-data/user = ${USER}/g" /usr/local/etc/php-fpm.d/www.conf
RUN sed -i "s/group = www-data/group = ${GROUP}/g" /usr/local/etc/php-fpm.d/www.conf

RUN apk add --no-cache \
    icu-dev \
    zlib-dev \
    libpng-dev \
    libzip-dev \
    ${PHPIZE_DEPS} \
    linux-headers

RUN docker-php-ext-install -j$(nproc) pdo pdo_mysql opcache pcntl intl exif gd zip \
    && pecl install -o -f redis-6.0.2 && pecl install -o -f xdebug-3.3.2 \
    && docker-php-ext-enable redis xdebug

COPY php/docker-php-ext-xdebug.ini "${PHP_INI_DIR}/conf.d"

WORKDIR /app

COPY --from=composer /usr/bin/composer /usr/bin/composer
COPY --chown=${USER} --from=composer-build /app/vendor /app/vendor
COPY --chown=${USER} --from=node-build /app/public /app/public
COPY --chown=${USER} src/ /app

COPY php/docker-entrypoint.sh /docker-entrypoint.sh

RUN chmod +x /docker-entrypoint.sh

USER ${USER}

RUN composer dump -o \
    && composer check-platform-reqs

ENTRYPOINT [ "/docker-entrypoint.sh" ]
