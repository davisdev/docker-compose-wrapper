FROM php:8.3-fpm-alpine

ENV GROUP=laravel
ENV USER=laravel

RUN adduser -g ${GROUP} -s /bin/sh -D ${USER}

RUN sed -i "s/user = www-data/user = ${USER}/g" /usr/local/etc/php-fpm.d/www.conf
RUN sed -i "s/group = www-data/group = ${GROUP}/g" /usr/local/etc/php-fpm.d/www.conf

RUN mkdir -p /app/public

RUN docker-php-ext-install pdo pdo_mysql opcache pcntl intl gd

COPY php/docker-entrypoint.sh /docker-entrypoint.sh

RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT [ "/docker-entrypoint.sh" ]