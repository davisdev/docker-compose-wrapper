FROM nginx:1.25.4-alpine

RUN mkdir -p /app/public

ADD nginx/default.prod.conf /etc/nginx/conf.d/default.conf